function getUserData() {
    const username = document.getElementById('username').value;
    const email = document.getElementById('email').value;
    const phone = document.getElementById('phone').value;
    const address = document.getElementById('address').value;

     // Create an object to store user data
    var userData = {
        username: username,
        email: email,
        phone: phone,
        address: address
    };

    // Reset errors and success message
    document.getElementById('usernameError').textContent = '';
    document.getElementById('emailError').textContent = '';
    document.getElementById('phoneError').textContent = '';
    document.getElementById('addressError').textContent = '';
    document.getElementById('successMessage').textContent = '';

    // Validation logic
    let isValid = true;

    if (!username) {
        document.getElementById('usernameError').textContent = 'Username is required.';
        isValid = false;
    }

    if (!email || !isValidEmail(email)) {
        document.getElementById('emailError').textContent = 'Email is required.';
        isValid = false;
    }

    if (!phone || !isValidPhone(phone)) {
        document.getElementById('phoneError').textContent = 'Phone number is required.';
        isValid = false;
    }

    if (!address) {
        document.getElementById('addressError').textContent = 'Address is required.';
        isValid = false;
    }

    // If all fields are valid, show success message
    if (isValid) {
        displayUserData(userData);
    }
}
// Can input only possitive number
document.getElementById('phone').addEventListener('keypress', function (event) {
    const keyCode = event.which;
    // Check if the pressed key is a positive number or backspace
    if (!(keyCode >= 48 && keyCode <= 57) && keyCode !== 8) {
        event.preventDefault();
        document.getElementById('phoneError').innerText = 'Please enter only positive numbers.';
    } else {
        document.getElementById('phoneError').innerText = '';
    }
});

function displayUserData(userData) {
    var usernameLabel = document.getElementById('usernameLabel');
    var emailLabel = document.getElementById('emailLabel');
    var phoneLabel = document.getElementById('phoneLabel');
    var addressLabel = document.getElementById('addressLabel');
    // Set the text content of the labels
    usernameLabel.textContent = userData.username;
    emailLabel.textContent = userData.email;
    phoneLabel.textContent = userData.phone;
    addressLabel.textContent = userData.address;
    document.getElementById('outputContainer').style.display = 'block';
}

function isValidEmail(email) {
    // Basic email validation
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailRegex.test(email);
}

function isValidPhone(phone) {
    // Basic phone number validation
    const phoneRegex = /^\d{10}$/;
    return phoneRegex.test(phone);
}